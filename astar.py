from multiprocessing import Process,Pool,Lock,Value,cpu_count
import magicwand
import reader
import bitmatrix
import field
import copy
import random
import time
import thread
import threading
import termios,sys,os

TERMIOS = termios
numCore = cpu_count()
decmap = {'1':1,'2':2,'3':5,'4':10,'5':20,'6':50,'7':100,'8':200,'9':500,'0':1000,'q':-1,'w':-2,'e':-5,'r':-10,'t':-20,'y':-50,'u':-100,'i':-200,'o':-500,'p':-1000}


def combineFields(image, stamp):
	temp = copy.deepcopy(image)
	for i in range(arrlen):
		temp.array[i] = temp.array[i] ^ stamp.array[i]
	return temp

def heuristics(first):
	s = 0
	for i in range(arrlen):
		s += bin(first.array[i]).count('1')
	return s

def compare(first, second):
	i = 0
	while(i < arrlen and first.array[i] == second.array[i]):
		i += 1
	if(i == arrlen):
		return 0

	if(first.array[i] > second.array[i]):
		return 1
	else:
		return -1

def insertIfNotExist(field, list):
	first = 0
	ll = len(list)
	count = ll

	while(count > 0):
		it = first
		step = count / 2
		it += step
		if(compare(list[it], field) == -1):
			first = it + 1
			count -= step + 1
		else:
			count = step

	if(first == ll):
		list.append(field)
		return 1
	else:
		if(compare(list[first], field) != 0):
			list.insert(first, field)
			return 1
	return 0

def existInClosedList(field, list):
	for i in range(len(list)):
		if(compare(list[i], field) == 0):
			return True
	return False

def getLowest(list):
	minIdx = -1
	minValue = LARGENUM

	for i in range(len(list)):
		if(list[i].starcost < minValue):
			minIdx = i
			minValue = list[i].starcost
	return minIdx

def isFinal(field):
	for i in range(arrlen):
		if(field.array[i] > 0):
			return False
	return True

def getTopMatch(field, list):
	idx = -1
	minValue = LARGENUM

	for i in range(len(list)):
		s = 0
		for j in range(arrlen):
			s += bin(list[i].array[j] ^ field.array[j]).count('1')
		if s < minValue:
			minValue = s
			idx = i
	return idx

def getBitMatrix(field):
	matrix = [[0 for i in range(maxy)] for j in range(maxx)]

	for i in range(arrlen):
		if(field.array[i] > 0):
			k = bin(field.array[i])[2:]
			t = len(k)
			while(t > 0):
				l = 63 - t
				x = positions[i][0] + (positions[i][1] + l) / maxy
				y = (positions[i][1] + l) % maxy
				matrix[x][y] = 1
				pos = k.find('1', 1, t)
				if(pos == -1):
					break
				k = k[pos:]
				t = len(k)
	return matrix

def getWeightedCoordinates(matrix, randomRange, x1, y1, x2, y2):
	x = []
	y = []
	totx = 0
	toty = 0
	for i in range(x1, x2):
		sum = 0
		for j in range(y1, y2):
			sum += matrix[i][j]
		totx += sum
		if(i - x1 > 0):
			x.append(x[-1] + sum)
		else:
			x.append(sum)
	for i in range(y1, y2):
		sum = 0
		for j in range(x1, x2):
			sum += matrix[j][i]
		toty += sum
		if(i - y1 > 0):
			y.append(y[-1] + sum)
		else:
			y.append(sum)

	crds = []

	for t in range(randomRange):
		temp = []

		xx = random.randint(0, totx)
		yy = random.randint(0, toty)

		for i in range(x1, x2):
			if(xx <= x[i - x1]):
				temp.append(i)
				break
		for i in range(y1, y2):
			if(yy <= y[i - y1]):
				temp.append(i)
				break
		crds.append(temp)
	return crds

def getCorrelation(matrix, x, y):
	temp = copy.deepcopy(matrix)
	ssx = 0.0
	ssy = 0.0
	ssxy = 0.0
	lx = min(maxx, x + smaxx)
	ly = min(maxy, y + smaxy)
	cor = 0

	for i in range(x, lx):
		for j in range(y, ly):
			if(temp[i][j] == 1):
				ssx += (i - x)**2
				ssy += (j - y)**2
				ssxy += (i - x)*(j - y)
			if(ssx * ssy !=0):
				temp[i][j] = (ssxy**2/(ssx*ssy))**0.5
	return temp

def getPrint(tempmatrix, x, y):

	i = min(stampN.value, 5)

	while (i > 0):
		i -= 1
		sp = orgStamps[i].array
		x1 = orgStamps[i].maxx
		y1 = orgStamps[i].maxy
		br = False
		bre = False
		ix = 0
		iy = 0
		for j in range(x1):
			for k in range(y1):
				if (not br):
					if (sp[j][k] == 1):
						ix = j
						iy = k
						br = True
				else:
					if (maxx > x + j - ix and x + j - ix > -1 and y + k - iy> - 1 and maxy > y + k - iy):
						if (tempmatrix[x + j - ix][y + k - iy] == 0) and (sp[j][k] == 1):
							bre = True
							break
			if (bre):
				break
		if (not bre):
			if (x - ix < 0):
				sx=abs(x - ix)
			else:
				sx = 0
			for z1 in range(max(0, x - ix), min(x - ix + x1, maxx)):
				if (y - iy < 0):
					sy = abs(y - iy)
				else:
					sy = 0
				for z2 in range(max(0, y - iy), min(y - iy + y1, maxy)):
					tempmatrix[z1][z2] = tempmatrix[z1][z2] ^ sp[sx][sy]
					sy += 1
				sx += 1
			return (str(orgStamps[i].stamp) + ' ' + str(y - iy) + ' ' + str(x - ix) + '\n')
def printAnswer(oList, cList):
	lowest = getLowest(oList)
	cur = oList[lowest]

	if(isFinal(cur) == False):
		tempmatrix = getBitMatrix(cur)

		for i in range(maxx):
			for j in range(maxy):
				if(tempmatrix[i][j] == 1):
					answer.append(getPrint(tempmatrix, i, j))

	while(cur.parent != -1):
		answer.append(str(cur.stamp) + ' ' + str(cur.stampy) + ' ' + str(cur.stampx) + '\n')
		cur = cList[cur.parent]

	answer.reverse()
	print 'Answer = ', len(answer)
	answer.insert(0, str(len(answer))+'\n')

	FILE = open("answer.txt","w")
	FILE.writelines(answer)
	FILE.close()

def getkey():
	fd = sys.stdin.fileno()
	old = termios.tcgetattr(fd)
	new = termios.tcgetattr(fd)
	new[3] = new[3] & ~TERMIOS.ICANON & ~TERMIOS.ECHO
	new[6][TERMIOS.VMIN] = 1
	new[6][TERMIOS.VTIME] = 0
	termios.tcsetattr(fd, TERMIOS.TCSANOW, new)
	c = None
	try:
		c = os.read(fd, 1)
	finally:
		termios.tcsetattr(fd, TERMIOS.TCSAFLUSH, old)
	return c

class GetOutOfLoop(Exception):
	pass

class keyPress(threading.Thread):
	def run(self):
		c = None
		while(c != 'c'):
			c = getkey()
			if (c == 'z'):
				print 'Command to change method to Monte Carlo'
				method.value = 3
                        elif (c == 'x'):
				print 'Command to Finish'
                                method.value = 4
			elif (decmap.has_key(c)):
				if (randVal.value-decmap[c]>0):
					randVal.value-=decmap[c]
				print 'MAX_RANDOM =',randVal.value
			elif (ord(c) == 91):
				if (stampN.value > 1):
					stampN.value -= 1
					print 'Stamp range decreased to = ',stampN.value
			elif (ord(c) == 93):
				if (stampN.value < numOrgStamps):
					stampN.value += 1
					print 'Stamp range increased to = ',stampN.value
		print 'Exit Program, GOODLUCK'

def bImageInsertion(field, stamp, stampx, stampy, stampnum, cost, parent):
	global head
	global DONE
	temp = combineFields(field, stamp)
	temp.stampx = stampx
	temp.stampy = stampy
	temp.stamp = stampnum
	temp.cost = cost
	temp.parent = parent
	temp.starcost = temp.cost + heuristics(temp)
	
	if(insertIfNotExist(temp, sQueue)):
		queue.append(temp)
		head += 1
		try:
			if(isFinal(temp)):
				raise GetOutOfLoop
		except GetOutOfLoop:
			DONE = True
			t = head
			lst = []
		
			lst.append(str(queue[head].cost) + '\n')
			while(1):
				lst.append(str(queue[t].stamp) + ' ' + str(queue[t].stampy) + ' ' + str(queue[t].stampx) + '\n')
				t = queue[t].parent
				if(t == 0):
					break

			FILE = open("answer.txt","w")
			FILE.writelines(lst)
			FILE.close()

			pass


def hImageInsertion(field, stamp, stampx, stampy, stampnum, cost, parent):
	temp = combineFields(field, stamp)
	temp.stampx = stampx
	temp.stampy = stampy
	temp.stamp = stampnum
	temp.cost = cost
	temp.parent = parent
	temp.starcost = temp.cost + heuristics(temp)

	if(not existInClosedList(temp, closedList)):
		insertIfNotExist(temp, openList)

def rImageInsertion(field, stamp, stampx, stampy, stampnum, cost, parent, pid):
	temp = combineFields(field, stamp)
	temp.stampx = stampx
	temp.stampy = stampy
	temp.stamp = stampnum
	temp.cost = cost
	temp.parent = parent
	temp.starcost = temp.cost + heuristics(temp)

	if(not existInClosedList(temp, rClosedList[pid])):
		insertIfNotExist(temp, rOpenList[pid])

def randomStamping(pid):

	endType = 0
	lowest = getLowest(rOpenList[pid])
	numRandom = MAX_RANDOM
	stampRange = numOrgStamps

	while(isFinal(rOpenList[pid][lowest]) == False):
		cur = rOpenList[pid].pop(lowest)
		print cur.starcost, pid
		rClosedList[pid].append(cur)
		pos = len(rClosedList[pid]) - 1
		t = getTopMatch(cur, randomStamps[pid])

		if(t != -1):
			rImageInsertion(cur, randomStamps[pid][t], randomStamps[pid][t].stampx, randomStamps[pid][t].stampy, randomStamps[pid][t].stamp, cur.cost + 1, pos, pid)

		tempmatrix = getBitMatrix(cur)

		for i in range(maxx):
			for j in range(maxy):
				if(tempmatrix[i][j] == 1):
					x = i
					y = j
					break
		blank = bitmatrix.BitMatrix(maxx, maxy)
		stamp = field.Field()
		stamp.array = r.get64BitConversionArray(r.applyField(blank, x, y, orgStamps[0]))
		rImageInsertion(cur, stamp, x, y, 0, cur.cost + 1, pos, pid)

		for i in range(numRandom):
			num = random.randint(0, stampRange - 1)
			x = random.randint(1 - orgStamps[num].maxx, maxx - 1)
			y = random.randint(1 - orgStamps[num].maxy, maxy - 1)

			blank = bitmatrix.BitMatrix(maxx, maxy)
			stamp = field.Field()
			stamp.array = r.get64BitConversionArray(r.applyField(blank, x, y, orgStamps[num]))
			stamp.stampx = x
			stamp.stampy = y
			stamp.stamp = orgStamps[num].stamp
			randomStamps[pid].append(stamp)
			rImageInsertion(cur, stamp, x, y, orgStamps[num].stamp, cur.cost + 1, pos, pid)

		px = maxx / 4
		py = maxy / 5

		xx = random.randint(0, 3)
		yy = random.randint(0, 4)

		x1 = xx * px
		y1 = yy * py
		x2 = (xx + 1) * px
		y2 = (yy + 1) * py
		if(x2 > maxx):
			x2 = maxx
		if(y2 > maxy):
			y2 = maxy

		co = getWeightedCoordinates(tempmatrix, numRandom, x1, y1, x2, y2)

		if(tempmatrix[co[0][0]][co[0][1]] == 1):
			x1, x2, y1, y2 = mwand.getPoints(co[0][0], co[0][1], tempmatrix)
			x, y = r.getTopCoordinate(tempmatrix, x1, y1, x2, y2)

			for num in range(stampRange):
				if(orgStamps[num].density > 0.6):
					blank = bitmatrix.BitMatrix(maxx, maxy)
					stamp = field.Field()
					stamp.array = r.get64BitConversionArray(r.applyField(blank, x - orgStamps[num].topx, y - orgStamps[num].topy, orgStamps[num]))
					stamp.stampx = x - orgStamps[num].topx
					stamp.stampy = y - orgStamps[num].topy
					stamp.stamp = orgStamps[num].stamp
					randomStamps[pid].append(stamp)
					rImageInsertion(cur, stamp, x - orgStamps[num].topx, y - orgStamps[num].topy, orgStamps[num].stamp, cur.cost + 1, pos, pid)

		for i in range(numRandom):
			num = random.randint(0, stampRange - 1)
			x = co[i][0]
			y = co[i][1]

			blank = bitmatrix.BitMatrix(maxx, maxy)
			stamp = field.Field()
			stamp.array = r.get64BitConversionArray(r.applyField(blank, x, y, orgStamps[num]))
			stamp.stampx = x
			stamp.stampy = y
			stamp.stamp = orgStamps[num].stamp
			randomStamps[pid].append(stamp)
			rImageInsertion(cur, stamp, x, y, orgStamps[num].stamp, cur.cost + 1, pos, pid)

		for i in range(numRandom):
			x = co[i][0]
			y = co[i][1]

			cormatrix = getCorrelation(tempmatrix, x, y)
			closestDiff = 3
			closestStamp = -1

			for j in range(1, stampRange):
				llx = min(x + orgStamps[j].maxx, maxx - 1)
				lly = min(y + orgStamps[j].maxy, maxy - 1)
				diff = abs(cormatrix[llx][lly] - orgStamps[j].cor)
				if(diff < closestDiff):
					closestDiff = diff
					closestStamp = j

			if(closestStamp != -1):
				blank = bitmatrix.BitMatrix(maxx, maxy)
				stamp = field.Field()
				stamp.array = r.get64BitConversionArray(r.applyField(blank, x, y, orgStamps[closestStamp]))
				stamp.stampx = x
				stamp.stampy = y
				stamp.stamp = orgStamps[closestStamp].stamp
				randomStamps[pid].append(stamp)
				rImageInsertion(cur, stamp, x, y, orgStamps[closestStamp].stamp, cur.cost + 1, pos, pid)

		if(method.value == 4):
			endType = 1
			break

		lowest = getLowest(rOpenList[pid])
		numRandom = randVal.value
		stampRange = stampN.value

	if(endType == 0):
		lock.acquire()

	return [rOpenList[pid], rClosedList[pid], endType]

def removeBigParts():
	global start
	global DONE
	counter = 0

	while True:
		tempmatrix = getBitMatrix(start)

		px = maxx / 4
		py = maxy / 5

		xx = random.randint(0, 3)
		yy = random.randint(0, 4)

		x1 = xx * px
		y1 = yy * py
		x2 = (xx + 1) * px
		y2 = (yy + 1) * py
		if(x2 > maxx):
			x2 = maxx
		if(y2 > maxy):
			y2 = maxy

		co = getWeightedCoordinates(tempmatrix, 1, x1, y1, x2, y2)

		maxclear = 10000000
		chosen = None

		if(tempmatrix[co[0][0]][co[0][1]] == 1):
			x1, x2, y1, y2 = mwand.getPoints(co[0][0], co[0][1], tempmatrix)
			x, y = r.getTopCoordinate(tempmatrix, x1, y1, x2, y2)
			height = x2 - x1 + 1
			width = y2 - y1 + 1

			for num in range(numOrgStamps):
				if(orgStamps[num].height < height + 2 and orgStamps[num].width < width + 2):
					blank = bitmatrix.BitMatrix(maxx, maxy)
					stamp = field.Field()
					stamp.array = r.get64BitConversionArray(r.applyField(blank, x - orgStamps[num].topx, y - orgStamps[num].topy, orgStamps[num]))

					temp = combineFields(start, stamp)
					temp.stampx = x - orgStamps[num].topx
					temp.stampy = y - orgStamps[num].topy
					temp.stamp = orgStamps[num].stamp

					if(heuristics(temp) < maxclear):
						maxclear = heuristics(temp)
						chosen = temp

			if(maxclear < heuristics(start) and chosen):
				start = chosen
				answer.append(str(chosen.stamp) + ' ' + str(chosen.stampy) + ' ' + str(chosen.stampx) + '\n')
				print heuristics(start)
				counter += 1

		if(method.value == 4):
			break

	tempmatrix = getBitMatrix(start)

	print counter

	for i in range(maxx):
		for j in range(maxy):
			if(tempmatrix[i][j] == 1):
				answer.append(getPrint(tempmatrix, i, j))

	print len(answer), ' <--------- answer '
	answer.insert(0, str(len(answer))+'\n')

	FILE = open("answer.txt","w")
	FILE.writelines(answer)
	FILE.close()
	DONE = True

def getPrint6(tempmatrix,x,y):
	i = stampN.value
	del superstamp[:]
	del superstampc[:]
	del superstampv[:]
	while (i>0):
		i-=1
		sp = orgStamps[i].array
		x1 = orgStamps[i].maxx
		y1 = orgStamps[i].maxy

		br = False
		bre = False
		ix = 0
		iy = 0
		num=0
		for j in range(x1):
			for k in range(y1):
				if (not br):
					if (sp[j][k] == 1):
						ix = j
						iy = k
						br = True
				else:
					if (maxx > x + j - ix and x + j - ix > -1 and y + k - iy> - 1 and maxy > y + k - iy):
						if (tempmatrix[x + j - ix][y + k - iy] == 0) and (sp[j][k] == 1):
							bre = True
						elif (sp[j][k] == 1):
							num+=1
			if (br):
				break
		if (not bre):
			superstamp.append(i)
			superstampv.append(num)
			superstampc.append([ix,iy])
	maxaa = 0
	maxii = 0
	for i in range(len(superstamp)):
		if (maxaa<superstampv[i]):
			maxaa = superstampv[i]
			maxii = i

	sp = orgStamps[superstamp[maxii]].array
	x1 = orgStamps[superstamp[maxii]].maxx
	y1 = orgStamps[superstamp[maxii]].maxy
	sx = 0
	if (x - superstampc[maxii][0] < 0):
		sx = abs(x - superstampc[maxii][0])
	else:
		sx = 0
	for z1 in range(max(0, x - superstampc[maxii][0]), min(x - superstampc[maxii][0] + x1, maxx)):
		if (y - superstampc[maxii][1] < 0):
			sy = abs(y - superstampc[maxii][1])
		else:
			sy = 0
		for z2 in range(max(0, y - superstampc[maxii][1]), min(y - superstampc[maxii][1] + y1, maxy)):
			tempmatrix[z1][z2] = tempmatrix[z1][z2] ^ sp[sx][sy]
			sy += 1
		sx += 1
	return (str(orgStamps[superstamp[maxii]].stamp)+' '+str(y-superstampc[maxii][1])+' '+str(x-superstampc[maxii][0])+'\n')

def printans():
	image = getBitMatrix(start)
	for i in range(maxx):
		for j in range(maxy):
			if(image[i][j] == 1):
				answer.append(getPrint6(image,i,j))

	print 'Answer = ',len(answer)
	answer.reverse()

	answer.insert(0, str(len(answer))+'\n')
	FILE = open("answer.txt","w")
	FILE.writelines(answer)
	FILE.close()

def finish(r):
	global lock

	if(r[2] == 0):
		printAnswer(r[0], r[1])
		try:
			pool.terminate()
		except Exception,e:
			print e
	else:
		rResults.append(r)

		if(len(rResults) == numCore):
			minValue = LARGENUM
			minPro = -1
			for i in range(numCore):
				lowest = getLowest(rResults[i][0])
				print rResults[i][0][lowest].starcost
				if(rResults[i][0][lowest].starcost < minValue):
					minValue = rResults[i][0][lowest].starcost
					minPro = i
			print minValue, minPro, '<-- min'
			printAnswer(rResults[minPro][0], rResults[minPro][1])

if __name__ == '__main__':
	global pool
	global head
	global DONE
	global lock
	global start
	global answer
	
	superstamp = []
	superstampv = []
	superstampc = []

	LARGENUM = 1000000000
	prevOpNum = 0
	answer = []
	openList = []
	closedList = []
	rResults = []
	randomStamps = [[] for i in range(numCore)]
	rOpenList = [[] for i in range(numCore)]
	rClosedList = [[] for i in range(numCore)]
	lock = Lock()
	method = Value('i', 0)

	r = reader.Reader()
	method.value = r.method
	numGenStamps = r.numGenStamps
	numOrgStamps = r.numOrgStamps
	genStamps = r.genStamps
	orgStamps = r.orgStamps
	arrlen = r.arrlen
	positions = r.pos
	start = r.startImage
	maxx = r.maxx
	maxy = r.maxy
	smaxx = r.smaxx
	smaxy = r.smaxy

	start.starcost = heuristics(start)

	MAX_RANDOM = int(raw_input("MAX_RANDOM = "))

	randVal = Value('i',0)
	stampN = Value('i',0)
	stampN.value = numOrgStamps
	randVal.value = MAX_RANDOM

	keyRetriever = keyPress()
	keyRetriever.start()

	mwand = magicwand.MagicWand(maxx, maxy, smaxx, smaxy)
	DONE = False
	method_changed = False

	while(not DONE):
		if(method.value == 1):
			queue = []
			sQueue = []
			queue.append(start)
			sQueue.append(start)
			head = 0
			tail = 0

			while(tail <= head and DONE == False):
				cur = queue[tail]
				for i in range(numGenStamps):
					bImageInsertion(cur, genStamps[i], genStamps[i].stampx, genStamps[i].stampy, genStamps[i].stamp, cur.cost + 1, tail)
				tail += 1

		if(method.value == 2):
			start.starcost = heuristics(start)
			insertIfNotExist(start, openList)
			lowest = getLowest(openList)

			while(isFinal(openList[lowest]) == False):
				cur = openList.pop(lowest)
				print cur.starcost
				closedList.append(cur)
				pos = len(closedList) - 1

				for i in range(numGenStamps):
					hImageInsertion(cur, genStamps[i], genStamps[i].stampx, genStamps[i].stampy, genStamps[i].stamp, cur.cost + 1, pos)

				lowest = getLowest(openList)
				if(method.value != 2):
					break
			if(method.value != 3):
				DONE = True
				printAnswer(openList, closedList)
			else:
				method_changed = True

		if(method.value == 3):
			if(not method_changed):
				start.starcost = heuristics(start)
				for i in range(numCore):
					insertIfNotExist(start, rOpenList[i])
			else:
				for i in range(numCore):
					rOpenList[i] = openList
					rClosedList[i] = closedList

			pool = Pool()
			presults = [pool.apply_async(randomStamping, args=(x,), callback = finish) for x in range(numCore)]
			pool.close()
			pool.join()
			DONE = True

		if(method.value == 5):
			removeBigParts()

		if(method.value == 6):
			printans()
			DONE = True
	print 'Done'