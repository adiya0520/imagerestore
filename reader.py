import field
import copy
import bitmatrix

FILE = open("problem0.txt", "r")
two_powers = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, 2147483648, 4294967296, 8589934592, 17179869184, 34359738368, 68719476736, 137438953472, 274877906944, 549755813888, 1099511627776, 2199023255552, 4398046511104, 8796093022208, 17592186044416, 35184372088832, 70368744177664, 140737488355328, 281474976710656, 562949953421312, 1125899906842624, 2251799813685248, 4503599627370496, 9007199254740992, 18014398509481984, 36028797018963968, 72057594037927936, 144115188075855872, 288230376151711744, 576460752303423488, 1152921504606846976, 2305843009213693952, 4611686018427387904]

class Reader():


	def __init__(self):
		startImage = self.readArray()
		finalImage = self.readArray()
		maxx = startImage.maxx
		maxy = startImage.maxy
		self.maxx = maxx
		self.maxy = maxy
		self.smaxx = 0
		self.smaxy = 0
		self.pos = self.getPositions()
		blank = bitmatrix.BitMatrix(maxx, maxy)

		startImage = self.applyField(startImage, 0, 0, finalImage)
		self.startImage = field.Field()
		self.startImage.array = self.get64BitConversionArray(startImage)		
		self.arrlen = len(self.startImage.array)

		numOrgStamps = int(FILE.readline())
		self.orgStamps = []
		self.genStamps = []
		print 'MaxX = ' + str(self.maxx) + ' MaxY = ' + str(self.maxy) + ' Stamps = ' + str(numOrgStamps)
		method = int(raw_input("Method = "))

		if(method == 1 or method == 2):
			for i in range(numOrgStamps):
				stamp = self.readArray()
				stamp.stamp = i
				self.orgStamps.append(stamp)
				for x in range(1 - stamp.maxx, maxx):
					for y in range(1 - stamp.maxy, maxy):	
						blank = bitmatrix.BitMatrix(maxx, maxy);
						temp = self.applyField(blank, x, y, stamp)
						f = field.Field()
						f.array = self.get64BitConversionArray(temp)

						#Herev ug tamganii ug daralt umnu ni baigaagui bol daraltiig nemne
						if(self.isExist(f) == False):
							f.stampx = x
							f.stampy = y
							f.stamp = i
							self.insertStamp(f)

		if(method == 3 or method == 5 or method == 6):
			for i in range(numOrgStamps):
				stamp = self.readArray()
				stamp.stamp = i
				self.orgStamps.append(stamp)
				if(stamp.maxx > self.smaxx):
					self.smaxx = stamp.maxx
				if(stamp.maxy > self.smaxy):
					self.smaxy = stamp.maxy

		self.method = method
		self.orgStamps.sort(self.cmp)
		self.numOrgStamps = len(self.orgStamps)
		self.numGenStamps = len(self.genStamps)

		i = 0
		while(i < self.numOrgStamps):			
			if(self.orgStamps[i].topx == -1):
				self.orgStamps.pop(i)
				self.numOrgStamps -= 1
				i = 0
			i += 1
		FILE.close()

	def readArray(self):
		cols = int(FILE.readline())
		rows = int(FILE.readline())
		array = bitmatrix.BitMatrix(rows, cols)

		counter = 0
		for i in range(rows):
			_col = FILE.readline()
			for j in range(cols):
				array.setPixel(i, j, _col[j])
				val = int(_col[j])
				if(val == 1):
					counter += 1

					if(i > array.vmaxx):
						array.vmaxx = i
					if(j > array.vmaxy):
						array.vmaxy = j
					if(i < array.vminx):
						array.vminx = i
					if(j < array.vminy):
						array.vminy = j

		array.topx, array.topy = self.getTopCoordinate(array.array, 0, 0, rows, cols)
		array.height = array.vmaxx - array.vminx + 1
		array.width = array.vmaxy - array.vminy + 1
		array.density = float(counter) / ((array.vmaxx - array.vminx + 1) * (array.vmaxy - array.vminy + 1))
		array.calcCorrelation()

		return array

	def getTopCoordinate(self, matrix, x1, y1, x2, y2):
		rows = x2 - x1
		cols = y2 - y1
		sumX = [[0 for j in range(cols)] for i in range(rows)]
		sumY = [[0 for j in range(cols)] for i in range(rows)]
		topRank = -1
		topX = -1
		topY = -1

		for i in range(rows):
			for j in range(cols):
				if(i == 0):
					sumX[i][j] = matrix[i + x1][j + y1]
				else:
					sumX[i][j] = sumX[i - 1][j] + matrix[i + x1][j + y1]
				if(j == 0):
					sumY[i][j] = matrix[i + x1][j + y1]
				else:
					sumY[i][j] = sumY[i][j - 1] + matrix[i + x1][j + y1]

		for i in range(rows):
			for j in range(cols):
				if(matrix[i + x1][j + y1] == 1):
					if(j == 0):
						left = 0
					else:
						left = sumY[i][j]
					if(j == cols - 1):
						right = 0
					else:
						right = sumY[i][cols - 1] - sumY[i][j]
					if(i == 0):
						top = 0
					else:
						top = sumX[i][j]
					if(i == rows - 1):
						bottom = 0
					else:
						bottom = sumX[rows - 1][j] - sumX[i][j]

					rank = min(left, right) + min(top, bottom)					

					if(rank > topRank):
						topRank = rank
						topX = i + x1
						topY = j + y1

		return (topX, topY)
		

	def getPositions(self):
		pow = 62
		pos = []
		temp = []
		for x in range(self.maxx):
			for y in range(self.maxy):
				if(pow == 62):
					temp.append(x)
					temp.append(y)
				pow -= 1
				if(pow == -1):
					temp.append(x)
					temp.append(y)
					pos.append(temp)
					temp = []
					pow = 62
		if(pos != 62):
			temp.append(self.maxx - 1)
			temp.append(self.maxy - 1)
			pos.append(temp)

		return pos

        def applyField(self, image, x, y, stamp):
		temp = image
                startx = max(0,x)
                starty = max(0,y)
                lastx = min(x + stamp.maxx, temp.maxx)
                lasty = min(y + stamp.maxy, temp.maxy)
				
                for i in range(startx, lastx):
                        for j in range(starty, lasty):
                                temp.array[i][j] = temp.array[i][j] ^ stamp.array[i-x][j-y]

		return temp

	def compare(self, first, second):
		i = 0
		while(i < self.arrlen and first.array[i] == second.array[i]):
			i += 1
		if(i == self.arrlen):
			return 0

		if(first.array[i] > second.array[i]):
			return 1
		else:
			return -1
			
	def insertStamp(self, stamp):
		left = 0
		right = len(self.genStamps) - 1
		#Herev tamganii list anh hooson bol shuud nemne
		if(len(self.genStamps) == 0):
			self.genStamps.append(stamp)
		#Herev orj irj bui tamga ni hamgiin ehend baigaa tamgaas baga bol ehend nemne
		elif(self.compare(stamp, self.genStamps[0]) == -1):
			self.genStamps.insert(0,stamp)

		#Herev orj irj bui tamga ni hamgiin suuld baigaa tamgaas ih bol suuld nemne
		elif(self.compare(stamp, self.genStamps[right]) == 1):
			self.genStamps.append(stamp)

		#Herev deerh 2 tohioldol bielehgui bol 2-tiin hailtaar bairiig olj bairluulna
		else:
			while(left <= right):
				x = (left + right) / 2
				if(self.compare(self.genStamps[x], stamp) == -1 and self.compare(self.genStamps[x+1], stamp) == 1):
					self.genStamps.insert(x+1, stamp)
					break
				if(self.compare(self.genStamps[x], stamp) == 1):
					right = x - 1
				elif(self.compare(self.genStamps[x+1], stamp) == -1):
					left = x + 1

	#2tiin hailtaar ug tamga omno ni hadgalagdsan baina uu? esehiig shalgaj baina
	def isExist(self, stamp):
		left = 0
		right = len(self.genStamps) - 1

		while(left <= right):
			x = (left + right) / 2
			if(self.compare(self.genStamps[x], stamp) == 0):
				return True
			if(self.compare(self.genStamps[x], stamp) == 1):
				right = x - 1
			elif(self.compare(self.genStamps[x], stamp) == -1):
				left = x + 1

		return False

        def get64BitConversionArray(self, matrix):
                array = []
                pow = 62
                sum = 0
                for x in range(matrix.maxx):
                        for y in range(matrix.maxy):
                                if(matrix.array[x][y] == 1):
                                        sum += two_powers[pow]
                                pow -= 1

                                if(pow == -1):
                                        array.append(sum)
                                        pow = 62
                                        sum = 0
                if(pow != 62):
                        array.append(sum)

                return array

	def cmp(self, first, second):
		if(first.maxx > second.maxx):
			return 1
		if(first.maxx < second.maxx):
			return -1
		if(first.maxy > second.maxy):
			return 1
		if(first.maxy < second.maxy):
			return -1
		return 0
