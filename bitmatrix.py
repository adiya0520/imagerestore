class BitMatrix:
	
	def __init__(self, _maxx, _maxy):
		self.maxx = _maxx
		self.maxy = _maxy
		self.array = [[0 for i in range(_maxy)] for j in range(_maxx)]
		self.cor = 0
		self.stamp = -1
		self.vmaxx = -1
		self.vmaxy = -1
		self.vminx = 10000
		self.vminy = 10000
		self.topx = -1
		self.topy = -1
		self.height = -1
		self.width = -1
		self.density = 0.0

	def setPixel(self, x, y, value):
		self.array[x][y] = int(value)

	def printMatrix(self):
		for i in range(self.maxx):
			print self.array[i][:]

	def calcCorrelation(self):
        	ssx = 0.0
	        ssy = 0.0
        	ssxy = 0.0

        	for i in range(0, self.maxx):
                	for j in range(0, self.maxy):
                        	if(self.array[i][j] == 1):
                                	ssx += i**2
	                                ssy += j**2
        	                        ssxy += i*j
		if(ssx*ssy != 0):
			self.cor = (ssxy**2/(ssx*ssy))**0.5
