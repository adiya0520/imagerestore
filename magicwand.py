import copy
class MagicWand():

	def __init__(self, maxx, maxy, smaxx, smaxy):
		self.lenY = maxy
		self.lenX = maxx
		self.smaxx = smaxx
		self.smaxy = smaxy
		self.moves = [[1, 0], [-1, 0], [0, 1], [0, -1]]
		self.bitpoint = []
		self.xlist = []
		self.value = []
		self.matrix = [[0 for i in range(self.lenY)] for j in range(self.lenX)]

	def floodfill(self, x, y):
		head = 0
		tail = 0
		self.bitpoint.append([x, y])
		self.matrix[x][y] = 0

		while(tail <= head):
			cur = self.bitpoint[tail]
			tail += 1
			for i in range(4):
				tempx = cur[0] + self.moves[i][0]
				tempy = cur[1] + self.moves[i][1]
				if(tempx >= 0 and tempx < self.lenX and tempy >=0 and tempy < self.lenY and abs(tempx - x) <= self.smaxx and abs(tempy - y) <= self.smaxy and self.matrix[tempx][tempy] == 1):
					self.bitpoint.append([tempx, tempy])
					self.matrix[tempx][tempy] = 0
					head += 1
			
	def getPoints(self, x ,y, _matrix):
		self.matrix = copy.deepcopy(_matrix)
		del self.bitpoint[:]
		self.floodfill(x, y)
		minx = x
		maxx = x
		miny = y
		maxy = y

		for i in range(len(self.bitpoint)):
			if self.bitpoint[i][0] < minx:
				minx = self.bitpoint[i][0]
			if self.bitpoint[i][0] > maxx:
				maxx = self.bitpoint[i][0]
			if self.bitpoint[i][1] < miny:
				miny = self.bitpoint[i][1]
			if self.bitpoint[i][1] > maxy:
				maxy = self.bitpoint[i][1]

		return (minx, maxx, miny, maxy)
